<?php

namespace App\Http\Controllers;

use App\Repositories\Team\TeamRepository;
use App\Resources\TeamResource;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    protected $teamRepo;

    public function __construct(TeamRepository $teamRepo)
    {
        $this->teamRepo = $teamRepo;
    }
    public function all()
    {
        return new TeamResource(Team::all());
    }

    public function find($id)
    {
        return response()->json($this->teamRepo->getById($id));
    }

    public function create(Request $request)
    {
        if ($request->isJson()) {
            $attributes = $request->json()->all();
        } else {
            $attributes = $request->all();
        }
        $this->teamRepo->create($attributes);
    }

    public function update($id, Request $request)
    {
        $team = $this->teamRepo->getById($id);
        $attributes = $request->all();
        if (isset($team))
        {
            $this->teamRepo->update($team, $attributes);
            return response()->json($team, 200);
        }
        return response('Team not found', 401);
    }

    public function delete($id)
    {
        $team = $this->teamRepo->getById($id);
        if (isset($team))
        {
            $this->teamRepo->delete($team);
            return response('Deleted Successfully', 200);
        }
        return response('Team not found', 401);
    }
}