<?php

namespace App\Http\Controllers;


use App\Project;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Team\TeamRepository;
use App\Resources\ProjectResource;
use App\Team;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    protected $projectRepo;
    protected $teamRepo;

    public function __construct(ProjectRepository $projectRepo, TeamRepository $teamRepo)
    {
        $this->projectRepo = $projectRepo;
        $this->teamRepo = $teamRepo;
    }
    public function all()
    {
        return new ProjectResource(Project::all());
    }

    public function find($id)
    {
        return response()->json($this->projectRepo->getById($id));
    }

    public function projects($team_id)
    {
        $team = $this->teamRepo->getById($team_id);
        /** @var Team $team */
        $projects = $team->projects;

        return response()->json($projects);
    }


    public function create(Request $request)
    {
        if ($request->isJson()) {
            $attributes = $request->json()->all();
        } else {
            $attributes = $request->all();
        }
        $this->projectRepo->create($attributes);

        return response('Project Created Successfully', 200);
    }

    public function update($id, Request $request)
    {
        $project = $this->projectRepo->getById($id);
        $attributes = $request->all();
        if (isset($project))
        {
            $project = $this->projectRepo->update($project, $attributes);
            return response()->json($project, 200);
        }
        return response('Project not found', 401);
    }

    public function delete($id)
    {
        $project = $this->projectRepo->getById($id);
        if (isset($project))
        {
            $this->projectRepo->delete($project);
            return response('Deleted Successfully', 200);
        }
        return response('Project not found', 401);
    }
}