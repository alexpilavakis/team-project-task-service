<?php

namespace App\Http\Controllers;

use App\Project;
use App\Resources\ProjectResource;
use App\Task;
use App\Team;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function test()
    {
        echo 'Hello World';
    }

    public function teams()
    {
        return json_decode(response()->json(Team::all())->content());
    }

    public function projects()
    {
        return new ProjectResource(Project::all()->keyBy->id);
        //return response()->json(Project::all());
    }

    public function tasks()
    {
        return json_decode(response()->json(Task::all())->content());
    }
}
