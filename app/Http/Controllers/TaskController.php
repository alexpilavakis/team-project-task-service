<?php

namespace App\Http\Controllers;


use App\Project;
use App\Repositories\Project\DbProjectRepository;
use App\Repositories\Task\DbTaskRepository;
use App\Resources\TaskResource;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $taskRepo;
    protected $projectRepo;

    public function __construct(DbTaskRepository $taskRepo, DbProjectRepository $projectRepo)
    {
        $this->taskRepo = $taskRepo;
        $this->projectRepo = $projectRepo;
    }
    public function all()
    {
        return new TaskResource($this->taskRepo->myProject());
    }

    public function tasks($project_id)
    {
        $project = $this->projectRepo->getById($project_id);

        /** @var Project $project */
        $tasks = $project->tasks;

        return response()->json($tasks);
    }

    public function find($id)
    {
        return response()->json($this->taskRepo->getById($id));
    }

    public function create(Request $request, $project_id)
    {
        /** @var Project $project */
        $project = $this->projectRepo->getById($project_id);
        if ($request->isJson()) {
            $attributes = $request->json()->all();
        } else {
            $attributes = $request->all();
        }
        $project->addTask($attributes);
        return response('Task Created Successfully', 200);
    }

    public function update(Request $request, $project_id, $task_id)
    {
        $task = $this->taskRepo->getById($task_id);
        $attributes = $request->all();
        if (isset($task))
        {
            $this->taskRepo->update($task, $attributes);
            return response()->json($task, 200);
        }
        return response('Task not found', 401);
    }

    public function delete($project_id, $task_id)
    {
        $task = $this->taskRepo->getById($task_id);
        if (isset($task))
        {
            $this->taskRepo->delete($task);
            return response('Deleted Successfully', 200);
        }
        return response('Task not found', 401);
    }
}