<?php

namespace App\Repositories\Project;
use App\Project;
use App\Repositories\DbRepository;


class DbProjectRepository extends DbRepository implements ProjectRepository
{

    protected $model;

    public function __construct(Project $model)
    {
        $this->model = $model;
    }

    public function myProjects()
    {
        return auth()->user()->team->projects;
    }

    public function addTask($attributes)
    {
        $this->model->tasks()->create($attributes);
    }

}