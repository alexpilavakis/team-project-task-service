<?php

use App\Team;
use Illuminate\Database\Seeder;

class StructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $team = new Team();
        $team->name = 'Team A';
        $team->team_leader_id = 1;
        $team->save();

        factory(App\Project::class, 2)->create()->each(function ($project) use ($team) {
            //associate project for team A
            $project->team()->associate($team)->save();
            factory(App\Task::class, 2)->create(['project_id'=>$project->id]);
        });

        $team = new Team();
        $team->name = 'Team B';
        $team->team_leader_id = 2;
        $team->save();

        factory(App\Project::class, 2)->create()->each(function ($project) use ($team) {
            //associate project for team B
            $project->team()->associate($team)->save();
            factory(App\Task::class, 2)->create(['project_id'=>$project->id]);
        });
    }
}
