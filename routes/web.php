<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/test', [
    'as'   => 'test',
    'uses' => 'ExampleController@test'
]);


$router->group(['prefix' => 'api/projects/','middleware' => 'client'], function () use ($router) {
    $router->get('',                ['uses' => 'ProjectController@all']);
    $router->get('{id}',            ['uses' => 'ProjectController@find']);
    $router->post('create',         ['uses' => 'ProjectController@create']);
    $router->delete('delete/{id}',  ['uses' => 'ProjectController@delete']);
    $router->put('edit/{id}',       ['uses' => 'ProjectController@update']);

    $router->get('/{project}/tasks',                    ['uses' => 'TaskController@tasks']);
    $router->get('/{project}/tasks/{id}',               ['uses' => 'TaskController@find']);
    $router->post('/{project}/add-task',                ['uses' => 'TaskController@create']);
    $router->delete('/{project}/tasks/delete/{task}',   ['uses' => 'TaskController@delete']);
    $router->put('/{project}/tasks/edit/{task}',        ['uses' => 'TaskController@update']);

});

$router->group(['prefix' => 'api/teams/','middleware' => 'client'], function () use ($router) {
    $router->get('',                ['uses' => 'TeamController@all']);
    $router->get('{id}',            ['uses' => 'TeamController@find']);
    $router->post('create',         ['uses' => 'TeamController@create']);
    $router->delete('delete/{id}',  ['uses' => 'TeamController@delete']);
    $router->put('edit/{id}',       ['uses' => 'TeamController@update']);

    $router->get('/{team}/projects',['uses' => 'ProjectController@projects']);
});
